# Projet BSN2 PROG02 

## Introduction

Vous allez devoir créer une application Web développée côté client avec React en JavaScript. Cette application permettra à l'utilisateur d'observer les données météorologiques d'une ville. 

Les points suivants seront abordés :

- Composants React
- Gestion des états et flux de données 
- Gestion de routes React
- Gestion de formulaire 
- Consommation d'une API 
- Affichage conditionnel
- Manipulation de tableaux et d'objets
- Utilisation d'un framework CSS

## Notation

La notation se basera sur :

- La propreté du code (indentation, respect des bonnes pratiques vues en cours)
- Utilisation de commentaires pertinents dans le code quand jugé nécessaire à la compréhension du code
- Nommage des variables et des fonctions aidant à la compréhension du code (Pas de franglais !! Français OU Anglais)
- Respect des conditions de rendu : projet sur GitLab, mon adresse en reporter et dernier commit avant le 17/12/21 23h59. 
- Respect des consignes du projet 
- Fonctionnement des étapes demandés
- Utilisation (le plus possible) des opérateurs JavaScript vu en cours
- un README avec :  
    * votre nom, votre prénom
    * les étapes pour compiler votre projet
    * le lien de votre application déployée sur Netlify
    * les étapes non réalisées ou réalisées de manière approximatives avec les explications des difficultées rencontrées


## Installation

Avant de pouvoir commencer à taper notre première ligne de code, la première étape va être d'installer notre environnement de développement.

- **IDE** : [Visual Studio Code](https://code.visualstudio.com/download) 

##### Sous Linux : 

- **Gestionnaire de paquet npm**:   
```sudo apt-get update```  
```sudo apt-get install nodejs npm ```

##### Sous Windows :

Télécharger et exécuter le Windows Installer -> [node](https://nodejs.org/en/download/)

## Extensions 
Une fois VSCode installé, ouvrez le puis allez dans les extensions   
(Ctrl + Shift + X ou directement depuis le menu à gauche)  
Cherchez puis installer le plugin **Simple React Snippets** qui va nous permettre de gagner du temps afin de ne pas taper des lignes de code redondantes. Vous utiliserez régulièrement :  
- **imrc** pour importer React (seul Component est vraiment nécessaire depuis les dernières màj)
- **cc** pour instancier une classe React


## Création du projet 


Ouvrez un terminal : celui de  VSCode (Terminal -> New Terminal) ou directement sur votre PC et assurez vous d'être placé où vous souhaitez que le dossier du projet soit créé.

On crée le projet [React](https://fr.reactjs.org/docs/create-a-new-react-app.html) : 
```bash 
npx create-react-app PROG02_VOTRENOM
cd PROG02_VOTRENOM
npm start
```

Une fois le **npm start** lancé, vous devriez voir une page web s'afficher en localhost:3000, c'est sur ce port que votre application se lancera par défaut. 

Maintenant que tout est prêt, ouvrez votre nouveau répertoire sur VSCode. (File -> Open Folder...)   
Il faut comprendre la structure de base des fichiers et leur utilité: 

- ***node_modules*** : packages géré par npm (librairies que vous utilisez)  
- ***public*** : assets et HTML de l'application
- ***src*** : c'est ici que se trouvent les fichiers sources de l'application que vous allez modifier/ajouter. Là où vous allez créer vos composants
    * App.js : composant fonctionnel qui est la page que vous voyez en local
    * App.test.js : permet de tester le composant App (nous ne verrons pas les tests dans ce module)
    * index.js : point d'entrée de notre application 
    * index.css : css du point d'entrée de l'application
    * reportWebVitals.js : fichier contenant des features servant à mesurer les performances de l'application 
    * setupTests.js : fichier chargé avant chaque test exécuté
- ***.gitignore*** : fichier permettant d'ignorer les dossiers/fichiers à ne pas push sur le gestionnaire de version utilisé
- ***package-lock.json*** : fichier automatiquement généré à chaque fois que les nodes_modules ou le package.json sont modifiés.
- ***package.json*** : toutes les librairies / scripts / description de votre projet se trouvent ici.
- ***README.md*** : fichier permettant de documenter votre projet qui sera visible sur le gestionnaire de version

## GitLab

Pour ce projet vous utiliserez le gestionnaire de version Git avec la plateforme GitLab.

Créez un compte sur GitLab avec votre adresse universitaire (-> [GitLab](https://gitlab.com/)), si vous avez déjà un compte GitHub cela devrait être lié. 

- Créez un nouveau projet (sélectionnez Create Blank Project), vous l'appelerez PROG02_VOTRENOM, vous laisserez la visibilité du projet en private et vous décocherez l'initialisation avec un read me. 
- Une fois validé, vous allez atterir sur une page qui contient plusieurs commandes, vous suivrez celles de "Push an existing folder" afin de pousser le projet react que vous avez créé précédemment. Une fois fait actualisez la page, vous devriez voir votre projet avec le contenu de ce que vous venez de pousser
- Sur votre projet, dans l'onglet de gauche allez dans le sous menu de Project information et cliquez sur Members. Ajoutez cette adresse suivante :  charlene.neves@outlook.fr et mettez le rôle "Reporter". Vous laisserez la date d'expiration vide. De cette manière je pourrai avoir accès à votre projet afin de pouvoir le compiler et le corriger à la fin.  

A chaque fois que vous aurez développé des étapes importantes dans votre projet, vous les commiterez sur votre repository.

Rappel :

- **git status**  (voir les fichiers qui ont été modifiés depuis le dernier commit)
- **git add .**  pour ajouter tous ces fichiers  ou **git add  suivi des noms de fichiers** à ajouter si vous ne voulez pas commiter toutes vos modifications.
- **git commit -m "votre message correspondant à ce commit"**
- **git push origin main**  pour pousser sur votre repository. 

Pour le reste des commandes ->  [Git Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)


## Etape 1 : Les Routes

Dans notre projet nous aurons deux "pages" qui mèneront sur deux routes différentes. Imaginons que nous souhaitons envoyer le lien de la météo d'une ville en particulière à quelqu'un : cela ne pourrait se faire sans route car on atterirait sur l'état de base de l'application qui sera ici l'accueil.

Suivez les étapes suivantes : 

- Créer un dossier **views** à la racine du dossier **src**, ici nous aurons les composants "conteneurs" de nos pages.
- Créer un dossier **components** à l'intérieur du dossier 
**src**, c'est là où vous créerez les composants "réutilisables" de présentation (qui auront pour ce projet un peu de logique).
- Afin de pouvoir créer des routes, on va avoir besoin du package react-router-dom, afin de l'installer: 
```
npm install react-router-dom --save
```  
- Dans le fichier App.js, supprimez le contenu du return, c'est ici que l'on va définir nos routes : nous avons besoin d'une route qui affichera le composant correspondant à l'accueil, et le composant correspondant à la météo d'une ville. (Vous créerez ces composants juste après). 



- Définir une route pour la page d'accueil qui aura pour exact path: **'/'**  et pour composant **Home** -> [Doc](https://reactrouter.com/web/example/basic)
- La page de météo aura pour path : **'/weather/:city**, (city étant un paramètre) et pour composant **WeatherCity**.  ->  [Doc](https://reactrouter.com/web/example/url-params) & [Doc2](https://reactrouter.com/web/api/Route/component).   
/!\ Vous utiliserez 'component' et non 'children'.
- On oublie pas d'importer les éléments du module que l'on utilise : 
```js 
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
```
A ce stade là si vous essayez un ```npm start``` cela ne fonctionnera pas car vous n'avez pas encore créer les composants dont les routes ont besoin. 

- Dans le dossier views créez vos composants Home et WeatherCity (n'oubliez pas les snippets pour aller plus vite), afin de tester vos routes faites juste afficher à l'intérieur de chacun d'eux :
```html 
<h1> {NOM_COMPOSANT} </h1>.
````
**Test :** Sur l'application, si vous lisez bien **Home** sur / et **WeatherCity** sur /weather/{nimportequelparamètre} alors vous pouvez continuer le projet.

## Etape 2 : Styliser l'application

Il existe beaucoup de moyens pour rendre une application à minima jolie et responsive. L'intérêt de ce module PROG02 n'étant en rien le Web Design seulement le minimum vous sera demandé lors de ce projet. 
C'est-à-dire :
- Une application web avec les éléments affichés proprement, et des couleurs à minima cohérentes.
- Une application web Responsive
- Un fichier css par composant, portant le nom du composant tous les deux dans le même dossier.   
(Exemple ->  Dossier : Home et Fichiers : Home.js et Home.css à l'intérieur).   
C'est une pratique couramment utilisée mais ça n'est pas la seule. 

Pour vous simplifier la tâche vous utiliserez un framework CSS comme [React-Bootstrap](https://react-bootstrap.github.io/getting-started/introduction/) ou bien encore la librairie [Material UI](https://mui.com/getting-started/usage/) pour React.
Les deux possèdent une section "components" qui vous permettent de prendre des composants déjà tout fait et de les adapter à votre besoin via certaines propriétés que vous trouverez dans la documentation.

## Etape 3 : Page Home

Vous allez maintenant modifier votre composant Home afin de le transformer en page d'accueil.  
**Cette page devra contenir au minimum :** 
- Un titre h1
- Un paragraphe p mentionnant "Projet BSN2 - Nom Prénom - PROG02"
- Un formulaire avec une input texte permettant de choisir la ville dont l'on veut la météo et un bouton pour valider.
- L'input aura pour placeholder la ville de votre choix. 


Tout ce dont vous aurez besoin pour créer le formulaire se trouve [ici](https://fr.reactjs.org/docs/forms.html). 

Lors de la validation de formulaire on souhaite être redirigé sur la deuxième route que l'on a créé avec le paramètre "city". Le [Redirect](https://reactrouter.com/web/api/Redirect) s'utilise uniquement dans le render (dans le return) d'un composant et pas dans une fonction.

- Créez un autre state qui contiendra le path de redirection.
- Faites un affichage conditionnel, si ce state est vide on affiche le contenu de la page Home, sinon on Redirect.

**Test :** Si vous tapez 'Lyon' dans l'input de votre formulaire, et qu'à la validation vous atterissez sur le composant WeatherCity avec le path /weather/Lyon alors vous pouvez continuer le projet.

## Etape 4

**La page WeatherCity (pour la partie 1) devra contenir :**

- Un titre h1 contenant "Météo {nomVille}" *(Si vos routes sont bien faites le paramètre {nomVille} se trouvera dans les props)*
- La date actuelle
- Un lien vers l'API avec une phrase assez explicite pour dire que les données du sites sont récupérées sur cette API

- Un bloc d'informations contenant :   
(*Essayez à minima de l'encadrer pour que cela fasse un bloc*) 
    - Le jour écrit complètement 
    - La température minimale de la journée
    - La température maximale de la journée
    - La ***moyenne*** de la vitesse du vent à 10m des 24h de la journée
    - La ***moyenne*** des précipitations des 24h de la journée
    - Une image affichant l'icône du temps

- Un bouton pour afficher le jour suivant 
- Un bouton pour afficher le jour précédent 

Toutes ces informations vont être récupérées via une API (excepté la ville).   
La donnée se requêtera sur un seul endpoint : https://www.prevision-meteo.ch/services/json/{nomVille} 
La documentation pour comprendre les données se trouve : [ici](https://www.prevision-meteo.ch/uploads/pdf/recuperation-donnees-meteo.pdf).

Vous pouvez utiliser **fetch** (natif à Javascript) ou **axios** afin de faire votre requête http.  
Si vous utilisez axios n'oubliez pas d'installer son package avec npm et d'importer son module dans le composant WeatherCity.
Cette requête doit être faite lorsque le composant est **monté**, vous stockerez la donnée dans un state.   
Une fois la donnée requêtée et dans votre state vous allez commencer par pouvoir afficher tout ce qui ne se trouve pas dans le bloc d'information (voir consigne au dessus), ainsi que les boutons.

Pour ce qui est du bloc d'information vous allez créer un composant WeatherCard dans le dossier component. La donnée que l'on a récupéré dispose des informations météréologiques de 5 jours  (fcst_day_0 -> jour actuel à  fcst_day_4). On va donc réutiliser le même composant WeatherCard. 

- Créer un state day, il servira à stocker un nombre entre 0 et 4 
- Dans le bouton suivant le state day doit s'incrémenter mais ne pas dépasser 4
- Dans le bouton suivant le state day doit décrémenter et ne pas passer en dessous de 0
- Vous passerez en props à WeatherCard la donnée récupérée (cela devrait donner quelque chose comme ça): 
``` js
<WeatherCard weather={this.state.weather['fcst_day_' + this.state.day]} /> 
```
N'oubliez pas qu'il s'agit d'un affichage conditionnel pour ce composant, la donnée doit avoir finie d'être requêtée ...

Pour le composant WeatherCard, afin de faire le calcul des moyennes des précipitation et des vents, il vous faudra utiliser les méthodes vues en cours (```map(), reduce()```). Comme la donnée que vous allez utilisez n'est pas un tableau mais un objet, pensez d'abord à utiliser ```Object.values()```.

## Etape 5 : Page WeatherCity - Partie 2

Pour la deuxième partie vous allez devoir afficher un tableau des détails par heure de la météo d'une journée. 

Le tableau contiendra les colonnes suivantes : 
- Heure
- Icone
- Température
- Vitesse du vent à 10m
- Précipitations (mm)

Vous pouvez abréger les noms, ou mettre des symboles, des icones si vous le souhaitez, l'important est que la donnée soit présente et le tableau clair.

Le tableau sera un composant réutilisable **WeatherDetails**. Il s'accordera avec le fonctionnement de **WeatherCard**, lorsque vous cliquerez sur jour suivant ou jour précédent cela doit changer les données. Pensez à votre state day.

Pour votre composant WeatherDetails, afin d'afficher dynamiquement la donnée vous aurez sûrement besoin des méthodes ```Object.entries()``` et ```map()```

## Etape 6 : Déployer votre application Web sur Netlify

Vous allez maintenant déployer votre application sur **Netlify**. 
Plusieurs plateformes comme **Netlify** existent (ex: Heroku) et permettent de déployer gratuitement dans la limite de certains besoins. Evidemment pour notre projet c'est largement suffisant. 

- Suivez ce [tutoriel](https://www.netlify.com/blog/2016/09/29/a-step-by-step-guide-deploying-on-netlify/) pour déployer votre site. L'inscription est obligatoire, vous pouvez directement vous inscrire avec votre compte Gitlab si vous préférez.   
(/!\ J'insiste sur le fait que jamais d'informations bancaires ne seront 
demandées)

Une fois votre application déployée, testez le lien généré pour voir si tout fonctionne bien. Le seul problème que vous devriez avoir est que si vous essayez de copier le lien de la météo d'une ville et essayez de cliquer dessus, vous allez avoir un 404 not found. 

Pour palier à ce problème :
- Créez un fichier **_redirects** dans le dossier **/public**
- Copiez-y le contenu suivant :
```
/* /index.html 200
```
- Pushez les dernières modifications sur votre repo Git
- Déployez à nouveau sur Netlify puis testez.

La solution est expliquée [ici](https://dev.to/dance2die/page-not-found-on-netlify-with-react-router-58mc).

## Etape 7 : Gestion d'erreur 

Jusqu'a présent si l'utilisateur tape n'importe quoi dans l'input ou tout simplement une ville qui n'est pas présente dans l'API, cela nous redirige tout droit sur une erreur. 
Vous devez gérer ce cas : 
- Si la ville n'existe pas, à la place du contenu habituel vous afficherez un message faisant comprendre à l'utilisateur que sa recherche est erronée. 
- Vous afficherez un [lien](https://reactrouter.com/web/example/custom-link) vers l'accueil.  
*(Vous pouvez également ajouter un retour vers l'accueil même si la recherche est correcte)*
---
***Vous pouvez abordez les bonus dans n'importe quel ordre.***

### Bonus 1    
Dans le tableau affichant la météo de  **la date du jour** : afficher seulement la météo des heures qui ne sont pas encore passées. (Ex: S'il est 16hxx vous afficherez les informations du tableau entre 16h et 23h).      

Indices: 
- Cela concerne donc uniquement le tableau du day 0, pensez à votre **state** day. 
- Pour récupérer l'heure avant le caractère H des données récupérées -> [split](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/String/split)

### Bonus 2
Sur la page Home, la gestion du formulaire n'est pas idéale.   
Pour améliorer ceci, si l'utilisateur ne tape aucune information dans l'input, vous afficherez un message d'erreur et il n'y aura bien évidemment pas de redirection tant que rien n'est saisi.

Pour le message d'erreur vous pouvez utiliser aussi bien des toasts (ex: avec une librairie supplémentaire), ou un simple message près du formulaire.

### Bonus 3 

Sur la page WeatherCity nous allons ajouter une carte qui zoomera sur la ville dont la météo est affichée. 
Pour cela vous utiliserez le package [react-leaflet](https://react-leaflet.js.org/docs/start-installation/).   
Pour la partie prerequisites vous nécessiterez (normalement) seulement d'importer  ```import 'leaflet/dist/leaflet.css'; ``` dans votre composant qui contiendra la carte.  
Vous disposez déjà des données nécessaires pour les coordonnées de la ville dans l'API que vous avez requêté. 

En résumé vous afficherez sur la page WeatherCity : 
- Une carte centrée sur les coordonnées de la ville dont la météo est affichée
- Un Marker dont la position sont les coordonnées de cette ville
- Une Popup lorsqu'on clique sur le Marker affichant la météo actuelle et le nom de la ville
- Vous laisserez le TileLayer par défaut dans l'exemple de la documentation. 

Si vous rencontrez des erreurs de compilation pour le module de leaflet une solution est proposée ->  [ici](https://stackoverflow.com/questions/67552020/how-to-fix-error-failed-to-compile-node-modules-react-leaflet-core-esm-pat)  

Si l'image du Marker n'apparait pas ->  [solution ici](https://stackoverflow.com/questions/49441600/react-leaflet-marker-files-not-found). 




