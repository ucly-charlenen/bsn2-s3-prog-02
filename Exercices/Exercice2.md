## Partie 1 (Rappel)

### 1. Ecrire cette fonction avec une boucle for

```js
const array = [1,2,3,4];
const newTab = array.map(num => num * 2);
```
*Résultat attendu* :
```json
[2, 4, 6, 8]
```

### 2. Retourner un tableau qui contient uniquement les nombres pairs de tab

```js
const tab = [1,2,3,4,5,6,7,8,9,10];
```
*Résultat attendu* :
```json
[2, 4, 6, 8, 10]
```
## Partie 2 

### 1. Retourner un tableau des âges de l'objet persons

```js
const persons = {
    0: {
        name: "Caroline",
        age: 23,
        height: 168
    },
    1: {
        name: "Axel",
        age: 27,
        height: 180
    },
    2: {
        name: "Ishak",
        age: 24,
        height: 175
    }
}
```
*Résultat attendu* :
```json
[23, 27, 24]
```

### 2. Créer une copie de l'objet Caroline où vous ajouterez une propriété hair avec la valeur 'brown' (Attention à ne pas modifier l'objet Caroline)
```js
const Caroline = {
    age: 23,
    height: 168
}
```
*Résultat attendu* :
```json
{
  age: 23,
  hair: "brown",
  height: 168
}
```

### 3. Concaténer les deux tableaux 
```js
const tab = [1,2,3];
const tab2 = [4,5,6,7];
```
*Résultat attendu* :
```json
[1, 2, 3, 4, 5, 6, 7]
```

## Partie 3
Afin d'utiliser axios dans jsfiddle.net il faut importer la librairie axios.
Sur la gauche cliquez sur 'Resources URL cdnjs' et copiez le lien suivant : 
https://unpkg.com/axios@0.16.2/dist/axios.js  puis cliquez sur le petit + bleu.

Soit l'api URL suivante : 
```js
const apiUrl = 'https://jsonplaceholder.typicode.com/' 
```
et les endpoints suivant :
```json
- GET	    /posts              -> récupère tous les posts

- GET	    /posts/{id}         -> récupère le post numéro {id}

- GET	    /posts/{id}/comments  -> récupère les commentaires du post numéro {id}

- POST	    /posts    -> créé un nouveau post,  l'endpoint prends en paramètre du body: 
{
  title: "",
  body: ""
  userId: 1
}
et en header le type a renvoyer (json) ainsi que l'encodage (ici UTF-8) (header nécessaire uniquement avec fetch ici)
```



### 1. Afficher le post avec l'id 2

*Résultat attendu* :
```json
{
  body: "est rerum tempore vitae
sequi sint nihil reprehenderit dolor beatae ea dolores neque
fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis
qui aperiam non debitis possimus qui neque nisi nulla",
  id: 2,
  title: "qui est esse",
  userId: 1
}
```

### 2. Afficher les adresses emails des commentaires de ce post (id2)

*Résultat attendu*:
```json
["Presley.Mueller@myrl.com", "Dallas@ole.me", "Mallory_Kunze@marie.org", "Meghan_Littel@rene.us", "Carmen_Keeling@caroline.name"]
```

### 3. Créer un nouveau post, et afficher la réponse de la requête 
*Résultat attendu*:
```json
{
  body: "test",
  id: 101,
  title: "Dev Web Avancé",
  userId: 1
}
```

### 4. Créer ce même post, mais cette fois les paramètres du body seront récupérés depuis un formulaire, et la requête sera envoyée lors de la validation de celui-ci. 
Indice: Afin d'éviter de déclencher l'envoi du formulaire vous utiliserez event.preventDefault(); Exemple :
```html
<form class="form-example" onsubmit="event.preventDefault(); return validateForm();">
```
*Résultat attendu*:
```json
Cf question 3
```


### 5. Ecrire une fonction asynchrone displayPost() qui récupère le post avec l'id2. Si la requête se passe bien, vous écrirez dans une div avec l'id 'post' le body de ce post. En cas d'échec on écrira la string 'error' dans cette div.
*Résultat attendu*:
```html
est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla
```
