## Partie 1

---

### 1. Transformer cette boucle avec un des opérateurs vu en cours :

```js
   const tab = [2, 6, 8];
   const newTab = [];
   for (let i = 0; i < tab.length; i++) {
        newTab.push(i * 2);
   }
```
*Résultat attendu :* 
```json
[4, 12, 16]
```

### 2. Transformer cette fonction en fonction fléchée :

```js
function sayHello(name) {
	console.log('Hello ' + name);
}

```
*Résultat attendu :* 
```json
"Hello Charlène"
```

## Partie 2

---

Soit la donnée suivante : 

```js
const videoGames = [
    {
        id: 1,
        name: "Yoshi Crafted World",
        price: {
            val: 59.99,
            currency: 'EUR'
        },
        platform: ['Switch'],
        testerReview: 15,
        customersReviews: [14, 15, 12, 9, 14]
    },
    {
        id: 2,
        name: "Rayman Legends",
        price: {
            val: 30.00,
            currency: 'EUR'
        },
        platform: ['Switch', 'PS4', 'Steam'],
        testerReview: 12,
        customersReviews: [9, 15, 12, 9, 14, 19, 6]
    },
    {
        id: 3,
        name: "Beat Saber",
        price: {
            val: 27.00,
            currency: 'USD'
        },
        platform: ['Quest', 'Rift', 'Steam', 'PS4'],
        testerReview: 18,
        customersReviews: [17, 10, 18, 20, 14]
    },
    {
        id: 4,
        name: "Zelda Breath Of The Wild",
        price: {
            val: 55.00,
            currency: 'USD'
        },
        platform: ['WiiU', 'Switch'],
        testerReview: 14,
        customersReviews: [17, 10, 18, 20, 14, 13, 19]
    },
    {
        id: 5,
        name: "Super Mario 64",
        price: {
            val: 45.00,
            currency: 'CHF'
        },
        platform: ['N64', 'DS', 'Switch'],
        testerReview: 13,
        customersReviews: [11, 18, 19, 17, 14]
    },
]
```

### 1. Retourner un tableau avec le nom des jeux 
*Résultat attendu :* 
```json
["Yoshi Crafted World", "Rayman Legends", "Beat Saber", "Zelda Breath Of The Wild", "Super Mario 64"]
```

### 2. Vérifier que tous les jeux ont au moins une note supérieure à 10 (testerReview)
*Résultat attendu :* 
```json
true
```

### 3. Vérifier qu'au moins un jeu est vendu en Franc Suisse 
*Résultat attendu :* 
```json
true
```

### 4. Retourner tous les jeux vidéos disponibles sur la PS4  
*Résultat attendu :* 
```json
[{
  customersReviews: [9, 15, 12, 9, 14, 19, 6],
  id: 2,
  name: "Rayman Legends",
  platform: ["Switch", "PS4", "Steam"],
  price: {
    currency: "EUR",
    val: 30
  },
  testerReview: 12
}, {
  customersReviews: [17, 10, 18, 20, 14],
  id: 3,
  name: "Beat Saber",
  platform: ["Quest", "Rift", "Steam", "PS4"],
  price: {
    currency: "USD",
    val: 27
  },
  testerReview: 18
}]
```

### 5. Calculer la moyenne des prix d'un jeu de Switch arrondie à deux décimales
*Résultat attendu :* 
```json
47.50
```

### 6. Retourner la moyenne des notes des clients du jeu "Rayman Legends"
*Résultat attendu:*

```json
12
```

### 7. Retourner le jeu avec la note (testerReview) la plus élevée
*Résultat attendu:*
```json
[{
  customersReviews: [17, 10, 18, 20, 14],
  id: 3,
  name: "Beat Saber",
  platform: ["Quest", "Rift", "Steam", "PS4"],
  price: {
    currency: "USD",
    val: 27
  },
  testerReview: 18
}]
```

### 8. Retourner un tableau de toutes les plateformes de la donnée videoGames, sans doublons
*Résultat attendu* 
```json 
["Switch", "PS4", "Steam", "Quest", "Rift", "WiiU", "N64", "DS"]
```