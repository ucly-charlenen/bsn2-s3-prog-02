## Installation

- Installer Nodejs (Windows Installer 64-bit version LTS) ->  [ici](https://nodejs.org/en/download/)
- Une fois installé ajoutez une variable d'environnement PATH avec pour valeur C:\Program Files\nodejs (la lettre peut varier si votre disque dur n'a pas celle-ci). Cette variable va nous permettre de pouvoir exécuter les commandes npm dans le terminal powershell.
- Une fois l'archive Exercice3 téléchargée et décompressée, ouvrez sur VSCode le dossier Exercice3.
- Ouvrez un terminal dans VSCode puis faites :  
```npm i```   Cette commande va nous permettre d'installer les nodes_modules  
Ensuite faites :  ```npm start```  afin de lancer le projet. Si tout se passe bien, la page web va s'ouvrir directement sur le port 3000.

Solution alternative, vous pouvez travailler directement ici :  https://stackblitz.com/edit/react-dsejrt?file=src%2FApp.js , attention à bien cliquer sur Fork avant de commencer à modifier. 

## 1. Créer un composant

En observant le fichier App.js vous verrez l'utilisation du module react-router-dom. On l'utilise ici pour définir un composant à afficher selon le path.

Vous trouverez cette ligne de code intentionnellement commentée afin de ne pas faire planter le programme. En effet le composant MyComponent n'existe pas encore et vous allez devoir le créer.

```jsx
{
  /* <Route path="/component" component={MyComponent} /> */
}
```

Dans le dossier **src** créez un fichier MyComponent.js dans lequel vous écrirez votre composant. Pour l'instant contentez-vous d'y afficher un simple Hello dans une div. N'oubliez pas de décommenter et d'importer votre composant une fois fait.

_Résultat attendu :_  
Cliquez sur le lien Go sur la page initiale, vous devez y voir un "Hello" s'afficher sur la route /component

## 2. Appel à une API

Dans MyComponent créez une fonction asynchrone getUsers().  
Faites un appel à l'api avec axios (importez juste le module il est déjà installé). Les informations de l'api sont les suivantes :  
**apiUrl :** https://dummyapi.io/data/v1  
**endpoint :** /user  
**headers :** {"app-id": "614a285b740286397770745e"}

Cette fonction devra être appelée immédiatement après que le composant soit monté. (Voir cycle de vie dans le cours)

Vous vous contenterez pour l'instant de vérifier la récupération des données dans la console du navigateur.

_Résultat attendu :_

```json
{data: Array(20), total: 99, page: 0, limit: 20}
```

## 3. Sauvegarder la donnée dans des states

Si on déroule la donnée obtenue dans la console on peut voir que chaque utilisateur a une propriété "title". Il y'a 4 valeurs possibles, on va s'intéresser pour l'exercice aux valeurs **"mr"** et **"mrs"**. Créez deux states **mrUsers** et **mrsUsers** que vous initialiserez avec un tableau vide.

Dans votre fonction getUsers, (à la place du console.log de la donnée), vous allez devoir mettre à jour les states mrUsers et mrsUsers. Dans le state mrUsers vous mettrez les utilisateurs qui ont pour title 'mr', et dans le state mrsUsers les utilisateurs qui ont pour title 'mrs'. Vous allez donc devoir filtrer la donnée.

Pour vérifier que vos states sont mis à jour avec la bonne donnée, vous pouvez les afficher dans la console du navigateur dans componentDidUpdate().

_Résultat attendu :_

```json
mrUsers (11) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]
mrsUsers (4) [{…}, {…}, {…}, {…}]
```

## 4. Créer un composant de présentation

Comme on l'a vu en React on utilise le pattern Conteneur - Présentation. Ici toute la logique nécessaire se trouve dans MyComponent. Maintenant on veut créer un composant réutilisable, qui va nous servir de composant de présentation.

Créer un composant ListUsers.js.

Son corps de rendu sera :

Une _div_ contenant un titre _h1_, et une liste _ul_.
Le titre _h1_ affichera la props **listName**.
A l'intérieur de la liste _ul_ vous allez parcourir la props **users** afin d'afficher pour chaque utilisateur un élément _li_ contenant "Prénom - Nom" correspondant aux propriétés firstName et lastName.

Importer le composant ListUsers dans MyComponent. A la place du Hello dans la div, appeler deux fois le composant ListUsers et passer lui les propriétés dont il a besoin.
(listName et users).
Vous donnerez un titre différent pour les deux listes dans la propriété listName, pour la propriété users vous passerez biensûr les states mrUsers puis mrsUsers.

*Résultat attendu :*

Sur la page /component vous devriez voir s'afficher deux listes.

## 5. Supprimer des utilisateurs

Dans le composant ListUsers, ajouter une input type button avec pour value "supprimer" en face de chaque utilisateur.
Ajouter une fonction onClick à cette input qui appelle la propriété fonction deleteUser qui passe en paramètre l'id du user à supprimer.

```html
<input type="button" value="supprimer" onClick={() =>
this.props.deleteUser(user.id)} />
```

Dans le composant MyComponent écrire une fonction deleteMrUser et deleteMrsUser. Elles prendront en paramètre un id. Le corps de fonction sera le suivant :
Je créé un nouveau tableau qui récupère tous les utilisateurs n'ayant pas l'id en paramètre.
J'update le state MrUser ou MrsUser selon la fonction.

Maintenant dans l'appel du composant ListUsers qui se trouve dans votre render() ajoutez lui la propriété deleteUser et qui aura pour valeur un pointeur sur deleteMrUser, et deleteMrsUser pour l'autre. N'oubliez pas de bind ces deux fonctions dans le constructeur :

```js
  this.deleteMrUser = this.deleteMrUser.bind(this);
  this.deleteMrsUser = this.deleteMrsUser.bind(this);
```

*Résultat attendu:*  
Quand vous cliquez sur supprimer à côté d'un utilisateur, il doit s'effacer de la liste.

Cette fonctionnalité correspond plutôt à une fonction "cacher" que supprimer, car ici on ne supprime la donnée que localement.
Pour supprimer il faudrait appeler l'API en DELETE, auquel cas la donnée ne reviendrait pas lors du refresh car elle serait véritablement supprimée.


