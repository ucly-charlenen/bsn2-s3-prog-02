## Partie 1 (Rappel)

### 1. 
```js
const array = [1,2,3,4];
let newTab = [];
for(var i=0; i<array.length; i++) {
  newTab.push(i*2);
}
```

### 2.
```js
const even = array.filter(n => n%2 === 0);
```
## Partie 2

### 1. 
```js
const ageTab = Object.values(persons).map(person => person.age);
```

### 2. 
```js
const Caroline2 = {...Caroline, hair :'brown'};
```

 ### 3.
 ```js
const tabMerged = [...tab,...tab2];
```


## Partie 3

Afin d'utiliser axios dans jsfiddle.net il faut importer la librairie axios.
Sur la gauche cliquez sur 'Resources URL cdnjs' et copiez le lien suivant : 
https://unpkg.com/axios@0.16.2/dist/axios.js  puis cliquez sur le petit + bleu.
### 1. 
```js
fetch("https://jsonplaceholder.typicode.com/posts/2")
  .then((response) => response.json())
  .then((json) => console.log(json))
  .catch((e) => console.log(e));

// ou

axios
  .get("https://jsonplaceholder.typicode.com/posts/2")
  .then((post) => console.log(post.data))
  .catch((e) => console.log(e));

  ```

  ### 2. 
  ```js
fetch("https://jsonplaceholder.typicode.com/posts/2/comments")
  .then((response) => response.json())
  .then((post) => console.log(post.map((comment) => comment.email)))
  .catch((e) => console.log(e));

// ou

axios
  .get("https://jsonplaceholder.typicode.com/posts/2/comments")
  .then((post) => console.log(post.data.map((comment) => comment.email)))
  .catch((e) => console.log(e));
  ```
  ### 3.
  ```js
fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  body: JSON.stringify({
    title: "Dev Web Avancé",
    body: "test",
    userId: 1,
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8",
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json))
  .catch((e) => console.log(e));

// ou

axios
  .post("https://jsonplaceholder.typicode.com/posts", {
    title: "Dev Web Avancé",
    body: "test",
    userId: 1,
  })
  .then((post) => console.log(post.data))
  .catch((e) => console.log(e));


  ```

  ### 4.
  ```html
<form class="form-example" onsubmit="event.preventDefault(); return validateForm();">
  <div class="form-example">
    <label for="user">UserId: </label>
    <input type="number" name="user" id="user" required>
  </div>
  <div class="form-example">
    <label for="title">title: </label>
    <input type="text" name="title" id="title" required>
  </div>
  <div class="form-example">
    <label for="body">Body: </label>
    <input type="text-area" name="body" id="body" required>
  </div>
  <div class="form-example">
    <input type="submit" value="Subscribe!">
  </div>
</form>
  ```
  ```js
function validateForm() {
  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      user: document.getElementById("user").value,
      title: document.getElementById("title").value,
      body: document.getElementById("body").value,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then((json) => console.log(json));
}

// ou

function validateForm() {
  axios
    .post("https://jsonplaceholder.typicode.com/posts", {
      user: document.getElementById("user").value,
      title: document.getElementById("title").value,
      body: document.getElementById("body").value,
    })
    .then((post) => console.log(post.data));
}

  ```
  ### 5.
  ```js
  // On pourrait toujours l'écrire avec fetch mais il faudrait encore retourner la promesse résolue en tant qu'objet JSON.
  async function displayPost () {
    try {
      const post2 = await axios.get('https://jsonplaceholder.typicode.com/posts/2');
      document.getElementById('post').innerHTML = post2.data.body;
    }
    catch(e) {
      document.getElementById('post').innerHTML = 'error';
    }
}

displayPost();
```
```html
<div id='post'></div>
```