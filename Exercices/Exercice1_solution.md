## Partie 1

1. 
```js
const newTab2 = tab.map(n => n*2); 
```

2.
```js
const sayHello = (name) => console.log('Hello ' + name);
sayHello('Charlène'); 
```


## Partie 2

1.
```js
const gamesNames = videoGames.map(game => game.name);
```

2. 
```js
const isGreatherThan10 = videoGames.every(game => game.testerReview > 10); 
```

3.
```js
const isCHF = videoGames.some(game => game.price.currency === 'CHF'); 
```

4.
```js
 const PS4Games = videoGames.filter(game => game.platform.includes('PS4'));
```

5.
```js
const switchGames = videoGames.filter(game => game.platform.includes("Switch"));
const avgGameOnSwitch =  switchGames.map(game => game.price.val).reduce((acc, price) => acc + price) / switchGames.length;
console.log(avgGameOnSwitch.toFixed(2)); 
```

6. 
```js
const rayman = videoGames.find(game => game.name === "Rayman Legends");
const avgRayman = rayman.customersReviews.reduce((acc,val) => acc + val) / rayman.customersReviews.length; 
```




7.
```js
const bestMark =  videoGames.map(game => game.testerReview).reduce((acc,grade) => Math.max(acc,grade));
// ou alors avec la syntaxe de décomposition : 
const bestMark = Math.max(...videoGames.map(game => game.testerReview))

const bestGame = videoGames.filter(game => game.testerReview === bestMark);
```

8.
```js
const listOfPlatform =  videoGames.map(game => game.platform).flat();
const filtered = [...new Set(listOfPlatform)];
```